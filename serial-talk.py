import sys
import os
import time

dir_path = os.path.dirname(os.path.realpath(__file__))
sys.path.insert(0, os.path.join(dir_path, 'packages'))

import serial

print("started...")

ix14 = serial.Serial()
ix14.port = "/dev/ttyUSB0"
ix14.baudrate = 115200
ix14.open()

for x in range(0, 10000):
    ix14.write(f"Hello{x}\n".encode())
    time.sleep(0.25)
