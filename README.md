# Brewer Science

### Digi [IX14](https://www.digi.com/products/networking/cellular-routers/industrial/digi-ix14) Serial Communication Issue

##### Seth Harris sharris@brewerscience.com

A brief README describing our goal and the current issue we are facing with the IX14.

**End Goal:**

![image not found](assets/endgoal.png)

For initial deployment we will be sending 1KB packets. Eventually we will move to upwards of 60KB.

**Issue:**

When listening to a serial port on IX14 using Python (v3.6.10), data loss and inconsistencies occur.

When we use the on board serial port monitor, we see 100% data consistency as shown in the video below.

![](assets/serial.mp4)

The issue occurs when we try reading the serial port with python on the IX14. For development purposes we are sending data to the IX14 via a python script.

See the video below for an example.

![](assets/python.mp4)

The scripts serial-talk.py & serial-listen.py were used. They are provided in the repository, along with a shell script that also failed.

**Other information:**

- Listening on /dev/ttymxc2 on the IX14
- Firmware: v20.2.162.90
- USB to RS-232
- DB-9 to RJ-45 [kit](https://www.digi.com/products/models/76000980)

**What we've tried:**

- All baud rates
- Hardware & software flow control
- Matching python and pyserial versions
- Updated to latest firmware
- Another scripting language (bash has same issues)
- Logging data to a file
- Disabling LAN and modem
- Running the script via SSH (interpreter and script)
- Running the script via scheduled tasks
