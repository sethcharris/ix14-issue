import sys
import os
import serial

dir_path = os.path.dirname(os.path.realpath(__file__))
sys.path.insert(0, os.path.join(dir_path, 'packages'))

print("started...")

file = open("/etc/config/scripts/iot-core/data.txt", "a")

macbook = serial.Serial()
macbook.port = "/dev/ttymxc2"
macbook.baudrate = 115200
macbook.open()

while True:
    print(macbook.readline())
